This project is concerned with using the colorectal cancer data set to find the relationship between color and shape characteristics. Characteristic extraction is used to then process and apply different machine learning algorithms.

The data set is described in the following link:
https://www.nature.com/articles/srep27988 